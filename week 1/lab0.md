Basic Exercises

`1.`	Once the virtual machine has completed booting, using the command pwd, what is the current working directory?

`/root/`

`2.`	How many files are contained within the home directory?  One simple way to do this is by using the ls command.

```
[root@localhost ~]$ ls | wc -l
5
```

`3.`	How many hidden files are contained within the home directory?  Without any arguments, the ls command doesn’t show hidden files.  Look at the man page for ls by running the command “man ls”.  You can navigate in the man page by using the up and down keys.

HINT: Hidden files in UNIX/Linux have a “.” in front of them.  For example, “.bash_history” is a hidden file.

```
[root@localhost ~]$ ls -a | grep "^\." | wc -l
13
```

I added another hidden directory, `.emacs.backups/`. I also saved some emacs customizations to put backups in that directory, which created `.emacs`, bringing my total up to 13.

`4.`	What directory would you expect to find the cp command located?

`/bin/` or maybe `/usr/bin/`.

```
[root@localhost ~]$ which cp
alias cp='cp -i'
        /bin/cp
```

`5.`	Where is the command to create a directory, mkdir, located on the filesystem?  What command did you use to find the command, and where it is located?  Give one alternative to the command you initially used to find mkdir.

`mkdir` is in `/bin`:

```
[root@localhost ~]$ which mkdir
/bin/mkdir
```

`which` is located in: `/usr/bin`:
```
[root@localhost ~]$ which which
alias which='alias | /usr/bin/which --tty-only --readi-alias --show-dot --show-tilde'
        /usr/bin/which
```

`6.`	Use the mkdir command to create a new directory under the root user’s home directory (e.g. /root), name it anything you’d like.  Create a file under that new directory using the touch command.  What does the contents of the new directory show?

```
[root@localhost ~]$ mkdir foo
[root@localhost ~]$ touch foo/bar
[root@localhost ~]$ ls foo
bar
```

`7.`	By default, the rm command will not remove directories.  You can use the flag -r to tell the remove command to remove recursively e.g. remove all files & directories under the directory being specified for removal (and the specified directory itself).  What happens when you run the command “rm” without -rf to remove the directory you created in #6?  What happens when you run the command “rm -rf” to remove the directory you created in #6?

```
[root@localhost ~]$ ls | grep foo
foo
[root@localhost ~]$ rm foo/
rm: cannot remove 'foo/': Is a directory
[root@localhost ~]$ rm -rf foo/
[root@localhost ~]$ ls | grep foo
```

When we just try `rm foo/`, we get an error message. With the `-rf` flags, `rm` successfully deletes `foo/` with no output.

`8.`	Print out the contents of /etc/passwd, which contains the list of users on the system in a very specific format.  This format is:
        username:password:user_id_number:group_id_number:full_name:home_directory:default_shell

Using a combination of the cat and cut command, only print out a list of just usernames.  Write out the series of commands below.

```
[root@localhost ~]$ cat /etc/passwd | cut -d : -f 1
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
uucp
operator
games
gopher
ftp
nobody
vcsa
saslauth
postfix
sshd
dbus
```

`9.`	Using a combination of the cat, cut, and tail command, only print out the username of the last user in the file.  Write out the series of commands below.

```
[root@localhost ~]$ cat /etc/passwd | cut -d : -f 1 | tail -n 1
dbus
```

`10`.	Using a combination of the cat, cut, and sort command only print out the usernames, sorted alphabetically, in descending order.  Write out the series of commands below.

```
[root@localhost ~]$ cat /etc/passwd | cut -d : -f 1 | sort
adm
bin
daemon
dbus
ftp
games
gopher
halt
lp
mail
nobody
operator
postfix
root
saslauth
shutdown
sshd
sync
uucp
vcsa
```


`11`.	Is the Debian Almquist Shell (dash) available on this virtual machine?  Is the Fish shell (fish) available? List two ways below that you can use to check the availability of a shell.

The Debian Almquist Shell is available at `/bin/dash`. Fish is not.

Using `which` to see if they are available:
```
[root@localhost ~]$ which dash
/bin/dash
[root@localhost ~]$ which fish
/usr/bin/which: no fish in (/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin)
```

Just trying to execute them to see if they are available:
```
[root@localhost ~]$ dash
# exit
[root@localhost ~]$ fish
-bash: fish: command not found
```

`12`.	What is the current value of the $PATH environment variable?  How would you modify it to add the directory /usr/local/bin?

Current value:

```
[root@localhost ~]$ echo $PATH
/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
```

Adding `/usr/local/bin`:
```
[root@localhost ~]$ PATH=$PATH:/usr/local/bin
[root@localhost ~]$ echo $PATH
/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin:/usr/local/bin
```

`13`.	Issue this command and explain the result. “> time; date >> time; cat < time”

```
[root@localhost ~]$ > time; date >> time; cat < time
Sat May 31 21:39:57 MDT 2014
```

The printed date seems to indicate the VM's clock is incorrect. The semicolon separates commands, so this command is the same as this sequence of commands:

```
[root@localhost ~]$ > time
[root@localhost ~]$ date >> time
[root@localhost ~]$ cat < time
```

* `> time` - creates a new file, `./time`. If there was a command before ">", that command's stdout would be redirected into the new file, but since there is no command, the file `./time` is empty
* `date >> time` - runs the command `date`, and appends its output to the contents of `./time`. Since `./time` had no contents, its only contents are the date.
* `cat < time` - prints the contents of `./time`. `<` is used to send the contents of `./time` to the program `cat` as input.


`14`.	Take a snapshot of the virtual machine, then run the command “rm / -rf” on your virtual machine.  What happened?  Restart the virtual machine (you may have to click Machine, then Reset), does it boot?

I received a warning message:

```
[root@localhost ~]$ rm -rf /
rm: it is dangerous to operate recursively on '/'
rm: use --no-preserve-root to override this failsafe
```

Trying to override the failsafe:

```
[root@localhost ~]$ rm -rf --no-preserve-root /
```

Tons of error messages of the form:

`rm: cannot remove '/.*': Permission denied`
`rm: cannot remove '/.*': Operation not permitted`

I sent an interrupt before it could finish trying to remove everything, `ls` no longer exists:

```
[root@localhost ~]$ ls
-bash: /bin/ls: No such file or directory
```

It does not boot. Error message (with the call trace removed):

```
Kernel panic - not syncing: Attempted to kill init!
Pid: 1, comm: init Not tainted 2.6.32-431.e16.x86_64 #1
Call Trace:
```
