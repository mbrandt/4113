#!/usr/bin/env bash

root_account=michaelbrandt5@gmail.com

subject="Disk Utilization Over Threshold"

# 80%, but leave off the percent character
threshold=80

# get the use % for the main drive, then remove the % char
usage=$(df / | tr -s " " | cut -d " " -f 5 | grep -v "^Use")
usage=${usage/"%"/}

message="Disk utilization is $usage%."

if [ $usage -gt $threshold ]
then
    echo $message | mail -s "$subject" $root_account
else
    echo $message
fi
