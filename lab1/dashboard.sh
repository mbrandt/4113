#!/usr/bin/env bash

echo "CPU AND MEMORY RESOURCES"
echo "------------------------"

free_memory_mb=$(free -m | grep "^Mem:" | tr -s " " | cut -d " " -f 4)
free_buffers_cache_mb=$(free -m | grep "^-/+" | tr -s " " | cut -d " " -f 4)
free_ram_mb=$((free_memory_mb + free_buffers_cache_mb))

cpu_load_average=$(uptime | tr -s " " | cut -d " " -f 9-11)

echo "Free RAM: ${free_ram_mb} MB"
echo "CPU Load Average: $cpu_load_average"
echo ""



echo "NETWORK CONNECTIONS"
echo "-------------------"

# generalize to more possible interfaces than just lo and eth0
interfaces=$(cut -d : -f 1 -s /proc/net/dev)

for interface in $interfaces
do
    values=$(grep $interface /proc/net/dev | cut -d : -f 2 | tr -s " ")
    bytes_received_index=1
    bytes_transmitted_index=9
    bytes_received=$(echo $values | cut -d " " -f $bytes_received_index)
    bytes_transmitted=$(echo $values | cut -d " " -f $bytes_transmitted_index)
    echo "$interface Bytes Received: $bytes_received"
    echo "$interface Bytes Transmitted: $bytes_transmitted"
done

# test connectivity by pinging google; if successful, exit code will be 0
ping -c 1 google.com > /dev/null 2>&1
if [ $? == 0 ]
then
    connectivity="Yes"
else
    connectivity="No"
fi

echo "Internet Connectivity: $connectivity"
echo ""



echo "ACCOUNT INFORMATION"
echo "-------------------"

 # one line per user
total_users=$(cat /etc/passwd | wc -l)

# usernames don't have spaces, so each word is a different logged in user
active_user_count=$(users | wc -w)

# uniq won't count lines together if they are in separate groups, so we need to
# sort the lines to get the correct total count, then we only want the name of
# the most frequently used shell, so cut is used to get just the shell names,
# then head takes the first one, so we can be sure this series of commands will
# always get the most frequently used shell
most_used_shell=$(cut -d : -f 7 /etc/passwd | sort | uniq -c | sort -nr | head -1 | tr -s " " | cut -d " " -f 3)

echo "Total Users: $total_users"
echo "Number Active: $active_user_count"
echo "Most Frequently Used Shell: $most_used_shell"
