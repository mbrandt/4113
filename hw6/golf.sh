#!/bin/bash

# Hole 5, Par 5
# Select a random user from /etc/passwd and check a remote host using SSH to determine if a user with the same UID exists. Print out the full line from passwd if the user exists, and nothing if it doesn’t.  You are allowed to use command substitution for this hole if needed.

# Hazard: Watch out, grep might match more than you bargained for!

ssh root@100.64.18.1 "awk -F : '\$3==$(cut -d : -f 3 /etc/passwd | shuf - n 1) {print}' /etc/passwd"

# strokes
# ssh (1) + awk (2) + cut (1) + shuf (1) = 5 strokes
