# add aliases to your ~/.bashrc for shortcuts to commands you frequently use,
# especially if there are arguments you like to always use, for example:

alias l='ls -hlABF'
alias gd='git diff --ignore--space-change'
