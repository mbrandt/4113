#!/usr/bin/env bash

# based on bash history, display 10 most frequently used commands

history | tr -s " " | cut -d " " -f 3 | sort | uniq -c | sort -nr | head -n 10
