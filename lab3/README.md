* boot into windows because apparently the vmware client stuff won't play nice with mac
* install openvpn with windows and connect to the vpn
* download the desktop vmware client
* connect to vcenter.int.colorado.edu, open up Brandt_MachineE
* restart the machine
* press escape to get to the grub screen
* press a, append the argument "single" to log in under single user mode
* now we're in, let's see what's in /
* `$ ls -la`
* I had to press enter after it listed the files, that's unusual, let's investigate `ls`:

```bash
$ which ls
/bin/ls
$ less /bin/ls
```

* well this doesn't look too friendly
* `$ emacs /bin/ls`
* time to remap caps lock to control (really wish I was working on this in Mac, the key is already mapped for me...)
* http://superuser.com/questions/334849/how-can-i-remap-capslock-on-windows-7-without-thirdparty-software
* regedit.exe, registry edited, apparently I have to reboot now
* start up openvpn gui and vmware client again
* reset the production machine
* alright, we're back
* `$ emacs /bin/ls`
* alias to the actual `ls` executable so I can keep working with it - `$ alias l=/bin/.ls -hlAFB --color`
* apparently the relocation is commented out, that's nice
* let's try just removing the bad `ls` and see if that works
* `$ rm -f /bin/ls`
* apparently it's a read-only filesystem
* `$ df -h` doesn't show a disk or anything mounted at `/bin/ls`
* looks like all the things are read-only, time to run `mount -o remount,rw /dev/mapper/VolGroup-lv_root   /`
* `$ rm -f /bin/ls`
* `$ mv /bin/.ls /bin/ls`
* the good `ls` is back in place
* time to change the password with `$ passwd`
* looks like it was succesful
* `$ reboot`
* login worked
* `$ less /bin/ls`
* the bad script appears to be gone
* `/etc/fstab` was changed
* `$ emacs /etc/fstab`, add line to mount `/dev/mapper/VolGroup-lv_root` automatically
* `$ reboot`
* looks good