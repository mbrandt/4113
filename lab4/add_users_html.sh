#!/bin/bash

# initialize the website directories, give it to webmasters group
mkdir -p /var/www/html/employees
chgrp webmasters -R /var/www/html
chmod 2774 -R /var/www/html

# create the users' html directory
users=(
    mscott
    dschrute
    jhalpert
    pbeesly
    abernard
    amartin
    kkapoor
    omartinez
    dphilbin
    tflenderson
    kmalone
    plapin
    shudson
    mpalmer
    cbratton
    mbrandt
)

for user in ${users[@]}; do
    mkdir /var/www/html/employees/$user
    chown $user /var/www/html/employees/$user
done
