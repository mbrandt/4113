#!/bin/bash

# the oldest a password can be
maxdays=90

users=(
    mscott
    dschrute
    jhalpert
    pbeesly
    abernard
    amartin
    kkapoor
    omartinez
    dphilbin
    tflenderson
    kmalone
    plapin
    shudson
    mpalmer
    cbratton
    mbrandt
)

for user in ${users[@]}; do
    chage -M $maxdays $user
done
