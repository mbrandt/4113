#!/bin/bash

# change the current umask, and update the default
umask 0017
sed -i 's/umask 022/umask 0017/' /etc/profile
