#!/bin/bash

# create group dirs in /home
groups=(
    accounting
    managers
    sales
    sup_rel
    webmasters
)

for group in ${groups[@]}; do
    mkdir /home/$group
    chgrp $group /home/$group/
    chmod 770 /home/$group
    chown nobody /home/$group
done
