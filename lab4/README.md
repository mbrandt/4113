# Lab 4

### Password policy
* Passwords must be changed every 90 days.
* Passwords must be at least 12 characters long, with at least one upper case character and at least one digit.

### notes from description
* primary groups
    * managers - full administrative access (but not root user) to A, B, C, D, and E
        * mscott
        * dschrute
        * jhalpert
    * website_managers - can restart httpd daemon on B, modify everything under /var/www
        * pbeesly
        * kkapoor
        * abernard
    * supplier_relations - can restart vsftpd on C, chown on C, modify everything under /var/ftp
        * mpalmer
* secondary groups - each group has their own dir on E under /home, only group can rwx those files
    * managers and supplier_relations (same as above)
    * reception
        * pbeesly
    * sales
        * abernard
        * plapin
        * shudson
        * dschrute
        * jhalpert
    * accounting
        * amartin
        * omartinez
        * kmalone
    * customer_service (not necessary to make this group)
        * kkapoor
    * warehouse (not necessary to make this group)
        * dphilbin
    * human_resources (not necessary to make this group)
        * tflenderson
    * quality_assurance (not necessary to make this group)
        * cbratton
    * sys_admins (not necessary to make this group)
        * mbrandt

* requirements by machine
    * UIDs / GIDs for the users and groups you create must be consistent across all servers
    * Machine A
        * managers group has full access without root password
        * umask 0017
        * password complexity and expiration policy is enabled for all users
    * Machine B
        * managers group has full access without root password
        * website group can restart httpd daemon with sudo, must enter their password
        * website group can modify /var/www/html (user apache must still be able to read everything)
        * each employee can modify their own /var/www/html/employees/$username
        * password complexity and expiration policy is enabled for all users
    * Machine C
        * managers group has full access without root password
        * supplier relations group can restart vsftpd daemon without password
        * supplier relations group can run chown without password
        * supplier relations group can modify /var/ftp
        * no one else can access C
        * umask 0017
        * password complexity and expiration policy is enabled for all users
    * Machine D
        * managers group has full access without root password
        * umask 0017
        * password complexity and expiration policy is enabled for all users
    * Machine E
        * managers group has full access without root password
        * /home/$group_name - only that group can read, write, modify files
                * new files and folders created under shared dir should retain group ID of parent dir, not of the user creating them
        * /home/$user - owner (user) and the group can read and write, everyone else denied
        * umask 0017
        * password complexity and expiration policy is enabled for all users

### practice on VM
* changing umask
    * set default umask based on recommendation from "Linux guru friend", `umask -S u=rwx,g=rw,o=`
    * running `umask` reveals this is the same as `umask 0017`
    * `reboot` to see if it persists
    * it didn't; need to modify `/etc/profile`; `sed -i 's/umask 022/umask 0017/' /etc/profile`
    * `reboot`, it works (and is unchanged in single-user mode)
    * run this on each machine: `umask 0017; sed -i 's/umask 022/umask 0017/' /etc/profile`

* adding groups (use -g to ensure consistency across machines)
    * `groupadd -g 500 managers`
    * `groupadd -g 501 webmasters`
    * `groupadd -g 502 sup_rel`
    * `groupadd -g 503 sales`
    * `groupadd -g 504 accounting`
    * `less /etc/group`, confirm groups were added successfully
    * add group directories
```bash
for group in accounting managers sales sup_rel webmasters; do
    mkdir /home/$group
    chgrp $group /home/$group/
    chmod 770 $group
done
```
* adding users
    * `useradd --gid managers --uid 500 mscott`

    * `useradd --gid managers --groups sales --uid 501 dschrute`
    * `useradd --gid managers --groups sales --uid 502 jhalpert`
    * `useradd --gid webmasters --uid 503 pbeesly`
    * `useradd --gid webmasters --groups sales --uid 504 abernard`
    * `useradd --gid accounting --uid 505 amartin`
    * `useradd --gid webmasters --uid 506 kkapoor`
    * `useradd --gid accounting --uid 507 omartinez`
    * `useradd --uid 508 dphilbin`
    * `useradd --uid 509 tflenderson`
    * `useradd --gid accounting --uid 510 kmalone`
    * `useradd --gid sales --uid 511 plapin`
    * `useradd --gid sales --uid 512 shudson`
    * `useradd --gid sup_rel --uid 513 mpalmer`
    * `useradd --uid 514 cbratton`
    * `useradd --uid 515 mbrandt`

* web files
    * `mkdir -p /var/www/html/employees`
    * `chgrp webmasters -R /var/www/html`
    * `chmod 2774 -R /var/www/html`, make new dirs always have the webmasters group
    * for each employee
        * `mkdir /var/www/html/employees/mscott`
        * `chown mscott /var/www/html/employees/mscott`

* give managers and my own account group root access on all machines
    * `emacs /etc/sudoers`
    * `%managers ALL=ALL` # this was given in class
    * `mbrandt ALL=ALL`

* give webmasters what they need
    * `ssh_b`
    * `emacs /etc/sudoers`
    * `%webmasters ALL=/etc/init.d/httpd restart`

* Meredith Palmer running vsftpd restart on C, chown on C without passwd, modify files under /var/ftp
    * description states she needs access because of supplier relations, give permissions to the group instead of user; if she leaves and a new supplier relations employee comes on, less overhead to get them the permissions they need
    * `emacs /etc/sudoers`
    * `%sup_rel ALL=/etc/init.d/vsftpd restart, NOPASSWD:chown`
    * `chgrp /var/ftp sup_rel`

### Machine A
* `less /etc/selinux/config`
* SELINUX is already set to disabled in the file
* `getenforce` returns "disabled"; SELINUX is already disabled
* set up aliases in `~/.bashrc` so I don't need to remember IP addresses of any machines any more
    * `alias ssh_b='ssh root@100.64.18.2'`
    * `alias ssh_c='ssh root@100.64.18.3'`
    * `alias ssh_d='ssh root@100.64.18.4'`
    * `alias ssh_e='ssh root@10.21.32.2'`
* `git clone https://mbrandt@bitbucket.org/mbrandt/4113-lab4.git`
* `cd 4113-lab4`
* `./a.sh`
* `umask 0017`, apparently this command didn't work from the script, but `sed` did
* `scp b.sh add_groups.sh add_users.sh add_users_html.sh 100.64.18.2:~`
* `scp c.sh umask.sh add_groups.sh add_users.sh 100.64.18.3:~`
* `scp d.sh umask.sh add_groups.sh add_users.sh 100.64.18.4:~`
* `scp e.sh umask.sh add_groups.sh add_users.sh add_group_dirs.sh 10.21.32.2:~`
* `emacs /etc/sudoers`, `%managers ALL=ALL`
* `emacs /etc/pam.d/system-auth`, add to the line with pam_cracklib.so
    * `minlen=15 ocredit=2 decredit=2 ucredit=2 minclass=2`
* `for server in 100.64.18.2 100.64.18.3 100.64.18.4 10.21.32.2; do scp chage.sh $server:~; done`
* `./chage.sh`
* `less /etc/shadow` to confirm change

### Machine B
* `less /etc/selinux/config`, `getenforce`, SELINUX is already disabled
* `./b.sh`
* should have checked for stuff in `/var/www/html`, chmod might have not done the right thing
* `emacs /etc/sudoers`
    * `%webmasters ALL=/etc/init.d/httpd restart`
    * `%managers ALL=ALL`
* `emacs /etc/pam.d/system-auth`, add to the line with pam_cracklib.so
    * `minlen=15 ocredit=2 decredit=2 ucredit=2 minclass=2`
* `./chage.sh`
* `less /etc/shadow` to confirm change

### Machine C
* `less /etc/selinux/config`, `getenforce`, SELINUX is already disabled
* remove the group and user "temp" from /etc/passwd and /etc/group
* `./c.sh`
* `umask 0017`, apparently this command didn't work from the script, but `sed` did
* `emacs /etc/sudoers`
    * `%sup_rel ALL=/etc/init.d/vsftpd restart, NOPASSWD:/bin/chown`
    * `%managers ALL=ALL`
* `chgrp -R sup_rel /var/ftp`
* `chmod 2770 /var/ftp`
* `emacs /etc/pam.d/system-auth`, add to the line with pam_cracklib.so
    * `minlen=15 ocredit=2 decredit=2 ucredit=2 minclass=2`
* `./chage.sh`
* `less /etc/shadow` to confirm change

### Machine D
* `less /etc/selinux/config`, `getenforce`, SELINUX is already disabled
* `./c.sh`
* `umask 0017`, apparently this command didn't work from the script, but `sed` did
* `emacs /etc/sudoers`, `%managers ALL=ALL`
* `emacs /etc/pam.d/system-auth`, add to the line with pam_cracklib.so
    * `minlen=15 ocredit=2 decredit=2 ucredit=2 minclass=2`
* `./chage.sh`
* `less /etc/shadow` to confirm change

### Machine E
* `less /etc/selinux/config`, `getenforce`, SELINUX is already disabled
* `umask 0017`
* `./e.sh`
* `emacs /etc/sudoers`, `%managers ALL=ALL`
* `emacs /etc/pam.d/system-auth`, add to the line with pam_cracklib.so
    * `minlen=15 ocredit=2 decredit=2 ucredit=2 minclass=2`
* `./chage.sh`
* `less /etc/shadow` to confirm change
