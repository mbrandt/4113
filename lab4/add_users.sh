#!/bin/bash

useradd --groups managers --uid 500 mscott
useradd --groups managers,sales --uid 501 dschrute
useradd --groups managers,sales --uid 502 jhalpert
useradd --groups webmasters --uid 503 pbeesly
useradd --groups webmasters,sales --uid 504 abernard
useradd --groups accounting --uid 505 amartin
useradd --groups webmasters --uid 506 kkapoor
useradd --groups accounting --uid 507 omartinez
useradd --uid 508 dphilbin
useradd --uid 509 tflenderson
useradd --groups accounting --uid 510 kmalone
useradd --groups sales --uid 511 plapin
useradd --groups sales --uid 512 shudson
useradd --groups sup_rel --uid 513 mpalmer
useradd --uid 514 cbratton
useradd --uid 515 mbrandt

users=(
    mscott
    dschrute
    jhalpert
    pbeesly
    abernard
    amartin
    kkapoor
    omartinez
    dphilbin
    tflenderson
    kmalone
    plapin
    shudson
    mpalmer
    cbratton
    mbrandt
)


for user in ${users[@]}; do
    chown 2700 /home/$user
done
