#!/bin/bash

# create the new groups
groupadd -g 500 managers
groupadd -g 501 webmasters
groupadd -g 502 sup_rel
groupadd -g 503 sales
groupadd -g 504 accounting
