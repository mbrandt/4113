commands

users - shows who is logged in to the machine
userdel - remove users
          -r delete the home directory recursively

usermod
useradd

w - see info on various users, their tty, time, idle, etc

quota filesystem option
- don't go over a factor of 2
- 1 TB storage, 200 users - 5GB quota/user
- expand quota, give each user 10GB instead, they all probably won't use all of it

who - like users

last
lastb - failed attempts to login

chage - change password aging

sudo/su - super user
su - switch user

whoami


why restrict access?
safety, control over what's happening, logging about who's doing what
protect files of different users from each other

what is a user
all its info information
- home directory
- user id (number associated with the account)
- permissions
- owner id stored in inode of the file
- group id
  - track different info by groups
- password
- shells
- gid
- name / text description of the use


create user accounts for daemons
- separate privileges
- keep daemons separate from each other (different account per daemon)

what is a group?
- same group ID
- intelligible name
- list of users

every file and dir
- user that owns it
- group that owns it
- everyone else / other

/etc/passwd shows primary group of the user, can belong to multiple groups
- does not show other groups that user is part of

su - alice # clears all env vars


default group is same as user

sources of user/group information
- /etc/passwd
- copy the password and shadow file all over the place
- PAM: pluggable authentication modules
  - talks to /etc/passwd, NIS, LDAP, MYSQL


permissions

chmod
file mode


umask
0002

everyone has read and execute
user and group have all

umask -S
u=rwx, g=rwx, o=rx

linux never automatically assigns execute

default permissions
files - 644
dirs - 775



cp
-a preserves permissions
otherwise default umask applies
owner is current user


permissions reserved for root
- mounting a filesystem
  - only root
- changing file or folder user & group ownership
  - `chown root .emacs`

- listening ont cp/udp ports lower than 1024
- ssh uses port 22


password
- way to authenticate
- shadow file

one-way hash
- in --> out
- seed/salt for the hash?

passwd - change password

prevent permissions from /etc/shadow
so they can't brute force the hashq

check health of /etc/shadow

# threats to password
exploit the person, social engineering
shoulder-surfing
keyboard logger
man-in-the-middle
brute force

# how do we mitigate these threats?
toss in a volcano? secure, but unusable
enforce pseudorandom passwords? forget passwords, or write it on a sticky note
- strong, memorable passwords that are changed periodically
   - maybe fail?
all accounts should expire
