nsservice
/etc/nsswitch.conf

pam is a c library

login uses unix system calls to pam for authentication
pam goes to nss
nss is just names

strace



ping -- retrieving ip address associated with name
record
every entry in dns is a record


A Record: address

dig - how you know dns is working


dig @oldduke.colorado.edu colorado.edu
send dns directly to old duke


ceo.colorado.edu
ceo - CNAME, cuengineeringonline.colorado.edu

receive cname, ask resolving name server for the A Record for cuengineeringonline.colorado.edu


can have more than NS records
names of authoritative domain servers for this domain


need auth name server for our website to be accessed by name
directnic.com (used to be more professional apparently); godaddy.com == drug lords


ns soa, associate name records
still need auth server running bind/other dns software


glue in 5 seconds
records that edu needs to have to point people to your auth dns server
ns, soa, a records


/etc/named.conf --> important path for lab 6
zone - carving out a section
tell bind there is a name, do something with it
type master -- auth server

create /var/named/localhost

$TTL at top tells bind to use that by default, so we don't have to set it for each record

rndc reload
rndc dumpdb - creates a file showing the cached names


make bind auth for southpark.co
change /etc/named.conf
add zone for southpark

resolv.conf should still point to local machine


/etc/resolv.conf
add "search colorado.edu" to try those names first


/etc/dhcp/dhcpd.conf
options
