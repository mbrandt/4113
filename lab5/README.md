* edit `/etc/dhcp/dhcpd.conf` on machine A, to add entry for machine F
* `ifconfig` on machine F to get MAC address; eth0 is not set up
* in vsphere client, edit settings for machine F
    * can't seem to change the network connection settings, but it does show the MAC address

	add this to /etc/dhcp/dhcpd.conf on machine A:

```
host MachineF {
    hardware ethernet 00:50:56:a0:5f:a8;
    fixed-address 100.64.18.5;
}
```

* on machine F, edit `/etc/sysconfig/network`
    * change HOSTNAME to be 'MachineF.dundermifflin.com'
* edit `/etc/sysconfig/network-scripts/ifcfg-eth0`
    * change ONBOOT to be 'yes'
* `service network restart`
* `ifconfig eth0 100.64.18.5` to change ip address (was set to 100.64.18.6)
* from machine A, can ping machine F with `ping 100.64.18.5`
* on machine F, `route add default gw 100.64.18.1 eth0`
* pinging google from machine F now works, can ssh to machine E from F

* `service network restart` on machine A, get edited `/etc/dhcp/dhcpd.conf` applied
* `service network restart` and `ifconfig` on machine F, still defaulting to 100.64.18.6 for ip address

* on machine F, `yum install -y httpd` to get apache/httpd installed
* machine F: `reboot`, maybe it will start up with ip 100.64.18.5 this time?

* on machine A: `service dhcpd restart`
* on machine F: `service network restart`

* now F is obtaining the proper IP address via DHCP based on settings in A, and can be reached via ssh from machines A through E
