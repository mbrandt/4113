$TTL 300
; dundermifflin.com.
dundermifflin.com.    SOA    chase.dundermifflin.com. postmaster.dundermifflin.com ( 2014072701
                                                                                     3600
                                                                                     1800
                                                                                     604800
                                                                                     300 )

dundermifflin.com.          NS       chase.dundermifflin.com.
www.dundermifflin.com.      CNAME    dundermifflin.com.
ftp.dundermifflin.com.      CNAME    platen.dundermifflin.com.
files.dundermifflin.com.    CNAME    roller.dundermifflin.com.
www2.dundermifflin.com.     CNAME    saddle.dundermifflin.com.

dundermifflin.com.    A    100.64.18.2
router 7d             A    100.64.0.18
carriage              A    100.64.18.2
platen                A    100.64.18.3
chase                 A    100.64.18.4
saddle                A    100.64.18.5
roller                A    10.21.32.2





*** WINNNING ***
options {
        recursion yes;
        directory "/var/named";
        listen-on { 127.0.0.1; 100.64.18.4; };
};

zone "localhost." {
        type master;
        file "localhost";
};

zone "dundermifflin.com." {
        type master;
        file "dundermifflin.com";
};
