#!/bin/bash

cut -d : -f 1,3 --output-delimiter=" " /etc/passwd | sort -t" " -k 2 -n | uniq -D -f 1
