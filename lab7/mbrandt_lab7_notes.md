# All Machines

### Install some stuff

```bash
yum install -y nc tcpdump nmap jwhois
```

### Set up some nice aliases in ~/.bashrc
```bash
# quickly see all the iptables info; putting --list as the last option also
# allows naming a specific chain to inspect, e.g. `ipt INPUT`
alias ipt='iptables --numeric --verbose --line-numbers --list'

# chains for blocking facebook and cheezburger contain tons of IPs, use this to
# quickly see just the default chains
alias iptd='ipt INPUT && echo "" && ipt OUTPUT && echo "" && ipt FORWARD'
```

### Apply the rules for all machines
```bash

# clear all active filter rules
service iptables stop

## Rule All-1: allow established and related connections, as well as traffic
## from lo
iptables --append INPUT -m conntrack --ctstate ESTABLISHED,RELATED --jump ACCEPT
iptables --append INPUT --in-interface lo --jump ACCEPT

## Rule All-2: allow incoming ICMP traffic only for ping and traceroute

iptables --new-chain icmp

for type in "echo-reply" "destination-unreachable" "echo-request" "time-exceeded"
do
    iptables --append icmp --protocol icmp --icmp-type $type --jump ACCEPT
done

iptables --append INPUT --protocol icmp --jump icmp

## Rule All-3: allow new SSH connections from the right scope

iptables --new-chain new-ssh

for ip in "100.64.0.0/24" "100.64.18.0/24" "100.64.254.0/24" "10.21.32.0/24"
do
    iptables --append new-ssh --source $ip --jump ACCEPT
done

iptables --append INPUT --protocol tcp --dport 22 -m conntrack --ctstate NEW --jump new-ssh

## Rule All-4: INPUT and FORWARD defaults should be DROP, OUTPUT should be
## ACCEPT
iptables --policy INPUT   DROP
iptables --policy FORWARD DROP
iptables --policy OUTPUT  ACCEPT
```

### After applying rules specific to the machine, back up existing iptables config, save the newly applied rules, and load them on startup
```bash
mkdir -p bak/etc/sysconfig/
cp /etc/sysconfig/iptables bak/etc/sysconfig/
iptables-save > /etc/sysconfig/iptables
```

# Machine A / Router

### Miscellaneous stuff for rules A-1 and A-2
```bash

# ensure that the response from `whois` gets accepted
iptables --policy INPUT   ACCEPT
iptables --policy FORWARD ACCEPT

# output from `whois` includes an `origin`; calling this function with `origin`
# as the argument sets `ips_from_origin` to the list of IP addresses associated with that
# origin
function get_ips_from_origin () {
    ips_from_origin=$(whois -h whois.radb.net '!g'$1 | grep -E "[0-9]\.[0-9]")
}
```

### Rule A-1: block all of the facebook.com IP addresses
```bash

# keep the facebook IPs in their own chains for more maintainability and reuse
iptables --new-chain fb-src
iptables --new-chain fb-dest

get_ips_from_origin AS32934 # origin taken from lab description
facebook_ips=$ips_from_origin
for ip in $facebook_ips
do
    iptables --append fb-src  --source      $ip --jump DROP
    iptables --append fb-dest --destination $ip --jump DROP
done

# specify the interfaces so that internal packets aren't checked against all the
# facebook ips
iptables --insert INPUT 3 --in-interface  eth0 --jump fb-src
iptables --append FORWARD --in-interface  eth0 --jump fb-src
iptables --append FORWARD --out-interface eth0 --jump fb-dest
iptables --append OUTPUT  --out-interface eth0 --jump fb-dest
```

### Rule A-2: block all of the cheezburger.com IP addresses
```bash
# keep the cheezburger IPs in their own chains for more maintainability and
# reuse
iptables --new-chain cb-src
iptables --new-chain cb-dest

# takes a set of origins (obtained from `whois`) and adds associated IPs to the
# chains holding all of the cheezburger IPs to be blocked
function block_cb_ips_from_origins() {
    for origin in $@
    do
        get_ips_from_origin $origin
        cheezburger_ips=$ips_from_origin
        for ip in $cheezburger_ips
        do
            iptables --append cb-src  --source      $ip --jump DROP
            iptables --append cb-dest --destination $ip --jump DROP
        done
    done
}

# dig cheezburger.com         # ip=168.62.179.64
# whois -h whois.radb.net $ip # origin=AS8075
#
# dig cheezburger.com         # ip=184.173.184.154
# whois -h whois.radb.net $ip # origin=AS36351
#
# dig cheezburger.com         # ip=216.176.177.72
# whois -h whois.radb.net $ip # origin=AS25700
block_cb_ips_from_origins AS8075 AS36351 AS25700

# specify the interfaces so that internal packets aren't checked against all the
# cheezburger ips
iptables --insert INPUT 3 --in-interface  eth0 --jump cb-src
iptables --append FORWARD --in-interface  eth0 --jump cb-src
iptables --append FORWARD --out-interface eth0 --jump cb-dest
iptables --append OUTPUT  --out-interface eth0 --jump cb-dest
```

### Rule A-3: include rules for the other machines for the event that their firewalls are dropped
```bash
machine_b=100.64.18.2
machine_c=100.64.18.3
machine_d=100.64.18.4
machine_e=10.21.32.2
machine_f=100.64.18.5

# Machine B & F
iptables --new-chain b-dest
iptables --new-chain f-dest

# Rule BF-1
iptables --append b-dest --protocol tcp --dport 80  --jump ACCEPT
iptables --append b-dest --protocol tcp --dport 443 --jump ACCEPT
iptables --append f-dest --protocol tcp --dport 80  --jump ACCEPT
iptables --append f-dest --protocol tcp --dport 443 --jump ACCEPT

# Rule All-3 for B and F
iptables --append b-dest --protocol tcp --dport 22 -m conntrack --ctstate NEW --jump new-ssh
iptables --append f-dest --protocol tcp --dport 22 -m conntrack --ctstate NEW --jump new-ssh

# Rule All-4 for B and F
iptables --append b-dest --jump DROP
iptables --append f-dest --jump DROP

# Machine C
iptables --new-chain c-src
iptables --new-chain c-dest

# Rule C-1 covered elsewhere

# Rule C-2
iptables --append c-dest --source 100.64.0.27 --protocol tcp --dport 20 -m conntrack --ctstate NEW --jump ACCEPT

# Rule C-4
iptables --append c-src --protocol udp --dport 20  --jump ACCEPT
iptables --append c-src --protocol tcp --dport 20  --jump ACCEPT
iptables --append c-src --protocol tcp --dport 80  --jump ACCEPT
iptables --append c-src --protocol tcp --dport 443 --jump ACCEPT

# Rule C-5 covered elsewhere

# Rule C-6
iptables --append c-src --jump DROP

# Rule All-3 for C
iptables --append c-dest --protocol tcp --dport 22 -m conntrack --ctstate NEW --jump new-ssh

# Rule All-4 for C
iptables --append c-dest --jump DROP

# Machine D
iptables --new-chain d-dest

# Rule D-1
iptables --append d-dest --protocol tcp --dport 53 --jump ACCEPT
iptables --append d-dest --protocol udp --dport 53 --jump ACCEPT

# Rule All-3 for D
iptables --append d-dest --protocol tcp --dport 22 -m conntrack --ctstate NEW --jump new-ssh

# Rule All-4 for D
iptables --append d-dest --jump DROP

# Machine E
iptables --new-chain e-dest

# Rule All-4 for E
iptables --append e-dest --jump DROP

# add the rules to the forwarding chain
iptables --append FORWARD --source      $machine_c --jump c-src
iptables --append FORWARD --destination $machine_b --jump b-dest
iptables --append FORWARD --destination $machine_c --jump c-dest
iptables --append FORWARD --destination $machine_d --jump d-dest
iptables --append FORWARD --destination $machine_e --jump e-dest
iptables --append FORWARD --destination $machine_f --jump f-dest

# Rule All-1 for each machine
iptables --insert FORWARD 1 -m conntrack --ctstate ESTABLISHED,RELATED --jump ACCEPT

# Rule All-2 for each machine
iptables --insert FORWARD 6 --protocol icmp --jump icmp
```


### Rule A-4 (overrides All-4): INPUT default should be DROP, OUTPUT and FORWARD defaults should be ACCEPT
```bash
iptables --policy INPUT   DROP
iptables --policy OUTPUT  ACCEPT
iptables --policy FORWARD ACCEPT
```

# Machine B / Carriage & Machine F / Saddle - Web Servers

### Apply the rules specific to this machine
```bash
## Rule BF-1: allow new HTTP and HTTPS connections regardless of the source
## address

iptables --new-chain http/s

iptables --append http/s --protocol tcp --dport 80  --jump ACCEPT
iptables --append http/s --protocol tcp --dport 443 --jump ACCEPT

iptables --insert INPUT 3 --protocol tcp --jump http/s
```


# Machine C / Platen - FTP Server

```bash
## Rule C-1: allow outgoing traffic on lo, as well as existing or established
## connections
iptables --append OUTPUT -m conntrack --ctstate ESTABLISHED,RELATED --jump ACCEPT
iptables --append OUTPUT --out-interface lo --jump ACCEPT

## Rule C-2: allow new FTP connections only from 100.64.N.0/24 and 100.64.0.27.

iptables --new-chain new-ftp

iptables --append new-ftp --source 100.64.0.27    --jump ACCEPT
iptables --append new-ftp --source 100.64.18.0/24 --jump ACCEPT

# --dport requires that a protocol be specified, and ftp can work over tcp or
# udp
iptables --insert INPUT 3 --protocol tcp --dport 20 -m conntrack --ctstate NEW --jump new-ftp
iptables --insert INPUT 4 --protocol udp --dport 20 --jump new-ftp

## Rule C-3: allow outgoing DNS requests to Machine D / Chase

iptables --append OUTPUT --protocol udp --destination 100.64.18.4 --dport 53 --jump ACCEPT
iptables --append OUTPUT --protocol tcp --destination 100.64.18.4 --dport 53 --jump ACCEPT

## Rule C-4: allow all outgoing FTP, HTTP and HTTPS connections

iptables --new-chain conn

iptables --append conn --protocol udp --dport 20 --jump ACCEPT
for port in 20 80 443
do
    iptables --append conn --protocol tcp --dport $port --jump ACCEPT
done

iptables --append OUTPUT --jump conn

## Rule C-5: allow outgoing ICMP traffic only for ping and traceroute

# just re-use the icmp chain created for Rule All-2
iptables --append OUTPUT --jump icmp

## Rule C-6: OUTPUT's default should be DROP
iptables --policy OUTPUT DROP
```

# Machine D / Chase - DNS Server

### Apply the rules specific to this machine
```bash
## Rule D-1: allow new DNS queries regardless of the source address

iptables --new-chain dns

iptables --append dns --protocol tcp --dport 53 --jump ACCEPT
iptables --append dns --protocol udp --dport 53 --jump ACCEPT

iptables --insert INPUT 3 --jump dns
```

# Machine E / Roller - File Server

### Apply the rules specific to this machine
```bash
## Rule E-1: new CIFS and SMB connections restricted to 10.21.32.0/24

iptables --new-chain share

# from lab description: The port numbers used by CIFS and SMB are: 135, 137-139,
# and 445.

iptables --append share --protocol tcp --dport 135  -m conntrack --ctstate NEW --jump ACCEPT
iptables --append share --protocol udp --dport 137 --jump ACCEPT
iptables --append share --protocol udp --dport 138 --jump ACCEPT
iptables --append share --protocol udp --dport 139 --jump ACCEPT
iptables --append share --protocol tcp --dport 445  -m conntrack --ctstate NEW --jump ACCEPT

iptables --append INPUT --source 10.21.32.0/24 --jump share

## Rule E-2: allow new SSH connections only from within 10.21.32.0/24 (overrides
## Rule All-3)

# first remove the rules set by the "All Rules" section
iptables --flush new-ssh

iptables --append new-ssh --source 10.21.32.0/24 --jump ACCEPT
```

# Miscellaneous

* [first time setting the rules on Machine A, I ran `iptables -P INPUT DROP`
  before ssh connections were added to the safe list](http://imgur.com/E2TsgAW)
