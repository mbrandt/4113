Michael Brandt

CSCI 4113 - UNIX System Administration

Final Project, option 4: Deploy a Trouble Ticket System

## Choosing a Trouble Ticket System

* ProjectSheet.pdf includes a link to [osTicket](http://osticket.com/), which is
  free, open source (with the code
  [on GitHub](https://github.com/osTicket/osTicket-1.8)), and can be set up to
  have some level of email integration
* a quick Google search shows osTicket as the top result, with a couple of other
  options
* [Trouble Ticket Express](http://www.troubleticketexpress.com/open-source-software.html)
  appears to havew fewer features than osTicket, and the web site is not as nice
* [Spiceworks](http://www.spiceworks.com/download/) is another top search
  result, but it's only for Windows
* [The 5 systems featured here](http://www.linuxplanet.com/linuxplanet/reports/7125/1)
  don't look as good as osTicket from a quick skim

Final choice: **osTicket**.

## Setting up the mail server

The project requirements mention that a mail server must be set up to be able to
handle the tickets by email. I will set up postfix on Machine B (Carriage).

```bash
[root@carriage ~]$ yum install -y mailx
[root@carriage ~]$ mkdir -p ~/bak/etc/postifx/; cp /etc/postfix/main.cf ~/bak/etc/postifx/
[root@carriage ~]$ emacs /etc/postfix/main.cf
```
* uncomment `myorigin = $mydomain`
* uncomment `inet_interfaces = $myhostname`
* comment `inet_interfaces = localhost`
* comment `mydestination = $myhostname, localhost.$mydomain, localhost`
* append `$mydomain`, and entries for each subdomain (ie,
      `$SUBDOMAIN.$mydomain`) to the `mydestination` definition
```bash
[root@carriage ~]$ postfix stop && postfix start
[root@carriage ~]$ mail jhalpert@dundermifflin.com
```

* mail is successfully delivered to /var/mail/jhalpert

## Deploying osTicket

osTicket requires PHP and MySQL to already be set up, and there must be a MySQL
database ready for osTicket to use.

### PHP

* download all the dependencies to fully support the osTicket features, and
  restart the apache server so it can serve the PHP:


```bash
[root@carriage ~]$ yum install -y php php-gd php-imap php-xml php-mbstring
[root@carriage ~]$ service httpd restart
```

### MySQL

* download the MySQL packages

```bash
[root@carriage ~]$ yum install -y mysql-server php-mysql
```

* Install MySQL, answering yes to each question as described
  [here](http://www.rackspace.com/knowledge_center/article/installing-mysql-server-on-centos):

```bash
[root@carriage ~]$ /usr/bin/mysql_secure_installation
```

* note that link also includes instructions for configuring iptables, which may
  be important for this to work alongside lab 7
* continue the MySQL setup, creating a database "tickets" that osTicket will be
allowed to use:
```bash
[root@carriage ~]$ service mysqld start
[root@carriage ~]$ chkconfig mysqld on
[root@carriage ~]$ mysql -u root -p
```
```MySQL
mysql> CREATE DATABASE tickets;
mysql> INSERT INTO mysql.user (User,Host,Password) VALUES('osticket','localhost',PASSWORD('**'));
mysql> FLUSH PRIVILEGES;
mysql> GRANT ALL PRIVILEGES ON 'tickets'.* to 'osticket'@'localhost';
mysql> exit
```

* database user "osticket" with full privileges for the database "tickets" is in
  place, all prerequisites for installing osTicket have now been met

### osTicket

* time to install osTicket itself, following
  [these instructions](http://osticket.com/wiki/Installation)

```bash
[root@carriage ~]$ curl -o ~/osTicket-v1.9.2.zip http://osticket.com/sites/default/files/download/osTicket-v1.9.2.zip
[root@carriage ~]$ unzip -d osTicket-v1.9.2 osTicket-v1.9.2.zip
[root@carriage ~]$ mkdir -p /var/www/html/support
[root@carriage ~]$ cp -ar osTicket-v1.9.2/upload/* /var/www/html/support/
```

* do rest of the installation through the browser, at
  [http://100.64.18.2/support/setup/](http://100.64.18.2/support/setup/) (now is
  not found since the setup is complete,
  [/support/](http://100.64.18.2/support/) works just fine)

* installer needs access to `include/ost-config.php`

```bash
[root@carriage ~]$ cd /var/www/html/support/include/
[root@carriage include]$ cp ost-sampleconfig.php ost-config.php
[root@carriage include]$ chmod 0666 ost-config.php
```
* after continuing with install process in the browser, we now need to revoke
  write permissions we gave earlier
```bash
[root@carriage include]$ chmod 0664 ost-config.php
```
* after logging in to [staff control panel](http://100.64.18.2/support/scp), I
  was told to remove the `setup/` dir
```bash
[root@carriage ~]$ rm -rf /var/www/html/support/setup/
```

* setup is complete!

## Testing the Installation

* created new ticket from user jhalpert at http://100.64.18.2/support/open.php,
  there is a new email in /var/mail/mbrandt with the subject "New Ticket Alert"
  * body of the message mostly looks like junk trying to view it in CentOS, but
    the subject is at least clear
* I respond with a comment on the ticket, new mail appears in /var/mail/jhalpert
  to let Jim know the ticket has been updated
* edit `/var/www/html/index.html`, update "technical support" link at bottom of
  the page to point to the `100.64.18.2/support/` so that anyone can access it
  without prior knowledge of the url
* added myself and dwight as sys admin users in the trouble ticket system
